var zeMdataJSON = 'https://spreadsheets.google.com/feeds/list/1QRj2L-lx2NuViS63sLMExntsvhy5-kmQGxJE-gFR75s/od6/public/values?alt=json'

var zeMstartDate;
var zeMendDate;

//kandidati
var zeMcolors = {
	'zeman': '#6a3d9a',
};

var zeMnameSurname = {
	'zeman': 'M. Zeman',
};

var zeMmakeBox = function(e) {
	console.log(e)
	d3.select('#timeline_zem')
		.append('div')
		.attr('id', 'infobox')
		.html('<h3 class="infotitle">' + e.gsx$titulek.$t + '</h3>'
			+ isPhoto(e.gsx$fotourl.$t)
			+ '<span class="infotext">'
			+ e.gsx$text.$t + '</span>')
		.style('top', '80px')
		.style('left', function() {
			return Object.keys(zeMcolors).indexOf(e.gsx$kandidat.$t) * 30 + 95 + 'px';
		})
		.style('top', function() {
			if (document.documentElement.clientWidth > 1006) {
				return dateDiff(zeMstartDate, e.gsx$datum.$t) * 3 - 40 + 'px';
			} else {
				return dateDiff(zeMstartDate, e.gsx$datum.$t) * 3 - 40 + 'px';
			}
		})
}

var zeMkillBox = function() {
	d3.select('#infobox').remove()
};

d3.json(zeMdataJSON, function(zeMdata) {
	zeMstartDate = zeMdata.feed.entry[0].gsx$datum.$t;
	zeMendDate = zeMdata.feed.entry.slice(-1)[0].gsx$datum.$t;

	var zeMdates = {};

	for (var kand in zeMcolors) { //filtr dat pro spojnicove linie
		zeMdates[kand] = [];
		var filtered = zeMdata.feed.entry.filter(function (ent) {
  			return ent.gsx$kandidat.$t == kand
		});
		for (var rec in filtered){
			zeMdates[kand].push(filtered[rec].gsx$datum.$t)
		}
	};

	var zeMsvgContainer = d3.select('#timeline_zem')
						.append('svg')
						.attr('width', function () {
							return Object.keys(zeMcolors).length * 25 + 75;
						})
						.attr('height', function () {
							return dateDiff(zeMstartDate, zeMendDate) * 3 + 200;
						});

	var zeMfullNames = zeMsvgContainer.append('g')
							.attr('class', 'fullNames')
							.selectAll('text')
							.data(Object.keys(zeMcolors))
							.enter()
							.append('text')
							.attr('class', 'fullName')
							.text(function (d, i) {
								return zeMnameSurname[d];
							})
							.style("text-anchor", "start")
							.style("font-size", '11px')
							.attr('x', function(d, i) {
								return Object.keys(zeMcolors).indexOf(d) * 25 + 85;
							})
							.attr('y', function(d, i) {
								return 100;
							})
							.attr('transform', function(d,i) {
								return 'rotate(-65, ' + ((Object.keys(zeMcolors).indexOf(d) * 25) + 85) + ', ' + 110 + ')'
							});


	var zeMconnectLine = zeMsvgContainer.selectAll('line') //vykresleni spojnic
							.data(Object.keys(zeMcolors))
							.enter()
							.append('line')
							.attr('class', 'connectline')
							.attr('x1', function(d, i) {
								return Object.keys(zeMcolors).indexOf(d) * 25 + 85;
							})
							.attr('y1', function(d, i) { //první datum v řadě
								return dateDiff(zeMstartDate, zeMdates[d][0]) * 3 + 130;
							})
							.attr('x2', function(d, i) {
								return Object.keys(zeMcolors).indexOf(d) * 25 + 85;
							})
							.attr('y2', function(d, i) { //poslední datum v řadě
								return dateDiff(zeMstartDate, zeMdates[d].slice(-1)[0]) * 3 + 130;
							})
							.attr('opacity', 1)
							.attr('stroke-width', 2)
							.style('stroke', function(d, i) {
								return zeMcolors[d];
							});

	var zeMdots = zeMsvgContainer.selectAll('circle')
							.data(zeMdata.feed.entry)
							.enter()
							.append('circle')
							.attr('r', 8)
							.attr('class', 'dot')
							.style('fill', function(d, i) {
								return zeMcolors[d.gsx$kandidat.$t];
							})
							.style('stroke-width', '1px')
							.style('opacity', 1)
							.attr('cx', function(d, i) {
								return Object.keys(zeMcolors).indexOf(d.gsx$kandidat.$t) * 25 + 85;
							})
							.attr('cy', function(d, i) {
								return dateDiff(zeMstartDate, d.gsx$datum.$t) * 3 + 130
							});

	zeMdots.on('mouseover', function(e) {
		zeMmakeBox(e);
	});

	zeMdots.on('mouseout', function(e) {
		zeMkillBox();
	});

	var zeMdatePoints = zeMsvgContainer.append('g')
							.attr('class', 'dates')
							.selectAll('text')
							.data(zeMdata.feed.entry)
							.enter()
							.append('text')
							.text(function (d, i) {
								return d.gsx$datum.$t;
							})
							.attr('class', 'date')
							.attr('x', function(d, i) {
								return 0;
							})
							.attr('y', function(d, i) {
								return dateDiff(zeMstartDate, d.gsx$datum.$t) * 3 + 130
							});

});

