var dataJSON = 'https://spreadsheets.google.com/feeds/list/1aCtrJICtceveM2aO6yn2_rFU1R_4QXVp6L6s25XKu_E/od6/public/values?alt=json'

var startDate;
var endDate;

//kandidati
var colors = {
	'zeman': '#6a3d9a',
	'horacek': '#1f78b4',
	'svejnar': '#ff7f00',
	'hilser': '#e31a1c',
	'ostatni': '#4d4d4d'
};

var nameSurname = {
	'zeman': 'Miloš Zeman',
	'horacek': 'Michal Horáček',
	'svejnar': 'Jan Švejnar',
	'hilser': 'Marek Hilšer',
	'ostatni': 'Ostatní'
};

var parseDate = function(dateString) {
	dateString = dateString.split(' ')
	var d = new Date();
	d.setTime(12 * 3600000)
	d.setDate(dateString[0])
	d.setMonth(dateString[1])
	d.setFullYear(dateString[2])
	return d
};

var dateDiff = function(start, end) {
	return (parseDate(end) - parseDate(start)) / (1000 * 60 * 60 * 24)
};

var isPhoto = function (fotourl) {
	if (fotourl != '') {
		return '<img class="infofoto" src="' + fotourl + '"></img>'
	} else {
		return '';
	}
};

var makeBox = function(e) {
	d3.select('#timeline')
		.append('div')
		.attr('id', 'infobox')
		.html('<h3 class="infotitle">' + e.gsx$titulek.$t + '</h3>'
			+ isPhoto(e.gsx$fotourl.$t)
			+ '<span class="infotext">'
			+ e.gsx$text.$t + '</span>')
		.style('top', '100px')
		.style('left', function() {
			return Object.keys(colors).indexOf(e.gsx$kandidat.$t) * 30 + 95 + 'px';
		})
		.style('top', function() {
			if (document.documentElement.clientWidth > 1006) {
				return dateDiff(startDate, e.gsx$datum.$t) * 3 - 40 + 'px';
			} else {
				return dateDiff(startDate, e.gsx$datum.$t) * 3 - 40 + 'px';
			}
		})
}

var killBox = function() {
	d3.select('#infobox').remove()
};

d3.json(dataJSON, function(data) {
	startDate = data.feed.entry[0].gsx$datum.$t;
	endDate = data.feed.entry.slice(-1)[0].gsx$datum.$t;

	var dates = {};

	for (var kand in colors) { //filtr dat pro spojnicove linie
		dates[kand] = [];
		var filtered = data.feed.entry.filter(function (ent) {
  			return ent.gsx$kandidat.$t == kand
		});
		for (var rec in filtered){
			dates[kand].push(filtered[rec].gsx$datum.$t)
		}
	};

	var svgContainer = d3.select('#timeline')
						.append('svg')
						.attr('width', function () {
							return Object.keys(colors).length * 25 + 75;
						})
						.attr('height', function () {
							return dateDiff(startDate, endDate) * 3 + 200;
						});

	var fullNames = svgContainer.append('g')
							.attr('class', 'fullNames')
							.selectAll('text')
							.data(Object.keys(colors))
							.enter()
							.append('text')
							.attr('class', 'fullName')
							.text(function (d, i) {
								return nameSurname[d];
							})
							.style("text-anchor", "start")
							.style("font-size", '11px')
							.attr('x', function(d, i) {
								return Object.keys(colors).indexOf(d) * 25 + 85;
							})
							.attr('y', function(d, i) {
								return 110;
							})
							.attr('transform', function(d,i) {
								return 'rotate(-65, ' + ((Object.keys(colors).indexOf(d) * 25) + 85) + ', ' + 110 + ')'
							});


	var connectLine = svgContainer.selectAll('line') //vykresleni spojnic
							.data(Object.keys(colors))
							.enter()
							.append('line')
							.attr('class', 'connectline')
							.attr('x1', function(d, i) {
								return Object.keys(colors).indexOf(d) * 25 + 85;
							})
							.attr('y1', function(d, i) { //první datum v řadě
								return dateDiff(startDate, dates[d][0]) * 3 + 130;
							})
							.attr('x2', function(d, i) {
								return Object.keys(colors).indexOf(d) * 25 + 85;
							})
							.attr('y2', function(d, i) { //poslední datum v řadě
								return dateDiff(startDate, dates[d].slice(-1)[0]) * 3 + 130;
							})
							.attr('opacity', 1)
							.attr('stroke-width', 2)
							.style('stroke', function(d, i) {
								return colors[d];
							});

	var dots = svgContainer.selectAll('circle')
							.data(data.feed.entry)
							.enter()
							.append('circle')
							.attr('r', 8)
							.attr('class', 'dot')
							.style('fill', function(d, i) {
								return colors[d.gsx$kandidat.$t];
							})
							.style('stroke-width', '1px')
							.style('opacity', 1)
							.attr('cx', function(d, i) {
								return Object.keys(colors).indexOf(d.gsx$kandidat.$t) * 25 + 85;
							})
							.attr('cy', function(d, i) {
								return dateDiff(startDate, d.gsx$datum.$t) * 3 + 130
							});

	dots.on('mouseover', function(e) {
		makeBox(e);
	});

	dots.on('mouseout', function(e) {
		killBox();
	});

	var datePoints = svgContainer.append('g')
							.attr('class', 'dates')
							.selectAll('text')
							.data(data.feed.entry)
							.enter()
							.append('text')
							.text(function (d, i) {
								return d.gsx$datum.$t;
							})
							.attr('class', 'date')
							.attr('x', function(d, i) {
								return 0;
							})
							.attr('y', function(d, i) {
								return dateDiff(startDate, d.gsx$datum.$t) * 3 + 130
							});

	$("text:contains('2. 6. 2016')").remove()
});

