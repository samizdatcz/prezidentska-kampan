---
title: "Horáček a spol. na startu. Co čeká prezidentské kandidáty?"
perex: "Do boje o Hrad se přihlásili první zájemci. Připomeňte si, čím musí projít"
description: "České prezidentské volby se konají až v lednu 2018, předvolební kampaň však prakticky začala už teď. Zatímco úřadující prezident Miloš Zeman oznámí svoje rozhodnutí o kandidatuře až příští rok v březnu, v posledních měsících se objevilo hned několik jeho možných oponentů. "
authors: ["Jan Cibulka", "Michal Zlatkovský"]
published: "14. listopadu 2016"
socialimg: https://interaktivni.rozhlas.cz/horacek-na-startu/media/socialimg.jpg
url: "horacek-na-startu"
libraries: [d3, jquery]
recommended:
  - link: https://interaktivni.rozhlas.cz/historie-brno/
    title: Volební mapa Brna: dominance lidovců dokáže přežít sto let a dvě diktatury
    perex: Politická paměť některých městských částí je nečekaně dlouhá. 
    image: https://interaktivni.rozhlas.cz/historie-brno/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/
    title: Krajské volby: Jak volili vaši sousedi?
    perex: Prohlédněte si nejpodrobnější mapu volebních výsledků
    image: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/media/socimg.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/od-tgm-k-zemanovi-poslechnete-si-vanocni-a-novorocni-projevy-vsech-prezidentu--1436738
    title: Od TGM k Zemanovi: Poslechněte si projevy všech prezidentů
    perex: V naší aplikaci najdete kompletní texty vánočních a novoročních projevů a u většiny z nich také zvukový záznam. 
    image: http://media.rozhlas.cz/_obrazek/3280833.jpeg
---

<aside class="small">
  <div id="timeline"></div>
</aside>

České prezidentské volby se konají až v lednu 2018, předvolební kampaň však prakticky začala už teď. Zatímco úřadující prezident Miloš Zeman oznámí svoje rozhodnutí o kandidatuře až příští rok v březnu, v posledních měsících se objevilo hned několik jeho možných oponentů. 

Nejvýraznější z nich, textař a bývalý spolumajitel sázkové kanceláře Fortuna Michal Horáček, po několika měsících náznaků definitivně oznámil svou kandidaturu na začátku listopadu. Záměr účastnit se voleb oznámil také lékař a občanský aktivista Marek Hilšer, o kandidatuře uvažuje i ekonom Jan Švejnar nebo předseda Akademie věd Jiří Drahoš. Český rozhlas přináší přehlednou časovou osu mapující zájemce o úřad prezidenta, kterou bude průběžně aktualizovat.

Aby se kandidáti mohli voleb zúčastnit, musí získat buď 50 tisíc podpisů občanů, nebo návrh nejméně dvaceti poslanců či deseti senátorů. Ministerstvu vnitra pak musí nejméně 66 dnů před termínem voleb odevzdat kandidátní listinu. Zákon, který podmínky průběhu přímé volby prezidenta upravuje, plánuje ale vláda změnit.

Důležitým omezením, které platný zákon stanoví, je maximální částka, kterou kandidáti mohou za kampaň utratit: 40 milionů v prvním kole a 10 milionů navíc pro případné druhé kolo. S penězi přitom musí nakládat pomocí speciálního volebního účtu se zákonem určenými pravidly. V jeho používání přitom během minulých prezidentských voleb [chybovali všichni prezidentští kandidáti](http://www.denik.cz/z_domova/v-kampani-chyboval-dle-senatniho-vyboru-kazdy-prezidentsky-adept-20131008.html).

## Cesta k přímé volbě

Zatímco na následující volby se uchazeči o prezidentské křeslo připravují až dva roky předem, ty předchozí, které proběhly v lednu 2013, nabraly spád podstatně rychleji. Šlo o vůbec první přímou volbu prezidenta, kterou po mnoha letech diskusí schválil Parlament v únoru 2012.

<aside class="small">
  <div id="timeline_zem"></div>
</aside>

Kandidátní listinu podalo celkem dvacet uchazečů, většina z nich přitom oznámila své rozhodnutí kandidovat v první polovině předvolebního roku 2012. Lhůta pro odevzdání kandidátních listin padla na listopad; do té doby ale podmínky zprvu splnilo jen osm kandidátů. Dalších devět nenasbíralo dostatečný počet podpisů - mezi nimi advokátka Klára Samková nebo aktivista a předseda strany Pravý blok Petr Cibulka. Ten si [neúspěšně stěžoval](http://www.lidovky.cz/vsichni-kandidati-na-prezidenta-jsou-rusti-fizlove-pise-cibulka-v-ustavni-stiznosti-gau-/zpravy-domov.aspx?c=A121230_152101_ln_domov_vs), že získal podporu více než 253 tisíc občanů, ale podpisové archy mu byly zadrženy na poště.

Tři další kandidáty - ekonoma Vladimíra Dlouhého, podnikatele Tomia Okamuru a bývalou europoslankyni Janu Bošíkovou - vyřadilo ministerstvo vnitra po namátkové kontrole podpisových archů. Zamítnutí uchazeči se následně obrátili na Nejvyšší správní soud. Ten v případě Jany Bobošíkové zjistil, že ministerstvo vnitra napočítalo ve dvou vzorcích podpisů chybovost 7,7 % a 11,5 %, čísla následně sečetlo a přisoudilo podpisům celkovou chybovost 19,2 %. Tím se Bobošíková dostala pod potřebnou hranici 50 tisíc podpisů. Soud rozhodl, že výpočet je neplatný, a nařídil Bobošíkovou dodatečně k volbám zaregistrovat.

Volební kampaň nabrala obrátky na podzim, tři měsíce před volbami. Předvolební debaty v režii České televize nebo [Českého rozhlasu](http://www.rozhlas.cz/zpravy/politika/_zprava/primy-prenos-prvni-duel-prezidentskych-finalistu--1162323) se konaly až v listopadu a prosinci. V průzkumech preferencí nejvíce bodovali dva expremiéři nominovaní občany, Jan Fischer a Miloš Zeman. Proti nim stáli čtyři "občanští" oponenti - kromě Jany Bobošíkové europoslankyně Zuzana Roithová, herečka Táňa Fischerová a skladatel Vladimír Franz - a tři kandidáti nominovaní poslanci a senátory: Jiří Dienstbier za ČSSD, Karel Schwarzenberg za TOP 09 a Přemysl Sobotka za ODS.

Z prvního kola, které se konalo 11. a 12. ledna 2013, překvapivě Jan Fischer nepostoupil. Proti Miloši Zemanovi, který skončil první s 24,2 % hlasů, se postavil Karel Schwarzenberg, s 23,4 % hlasů druhý. Ve dvou týdnech mezi prvním a druhým kolem se kampaň nebývale vyostřila. Voliči se rozdělili na dva vyhraněné tábory a komentátoři psali o rozdělené společnosti. Volby nakonec vyhrál poměrem 54,8:45,2 Miloš Zeman. Z následné [analýzy](https://ihned.cz/c3-59198510-000000_d-59198510-vysledky-druhe-kolo-data) vyplynulo, že zatímco Schwarzenberga volili ve velké míře lidé s vysokoškolským vzděláním, studenti a obyvatelé velkých měst, Zeman bodoval v menších obcích a u voličů se středoškolským a nižším vzděláním.

Druhá přímá prezidentská volba se kvůli kandidatuře Michala Horáčka stává tématem už dnes, rok a čtvrt před samotným volebním termínem. Délkou předvolebního období se tak české volby začínají podobat těm americkým. Kolik kandidátů se objeví a jak vyhrocený bude boj mezi nimi, je zatím otevřené. Zpravodajský web Českého rozhlasu jej bude i prostřednictvím průběžně doplňované časové osy sledovat.